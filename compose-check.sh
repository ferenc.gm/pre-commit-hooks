#! /bin/bash
# Verifies that files passed in are valid for docker-compose
set -e

check_files_combined() {
    local file_switches=$1
    local cmd="docker-compose ${file_switches} config --quiet 2>&1"
    eval "${cmd}" | sed "/variable is not set. Defaulting/d"
    return "${PIPESTATUS[0]}"
}

check_files() {
    local all_files=( "$@" )
    file_switches=""
    has_error=0
    for file in "${all_files[@]}" ; do
        if [[ -f "${file}" ]]; then
            file_switches="--file ${file} ${file_switches}"
        fi
    done

    if [[ -n "${file_switches}" ]]; then
        if ! check_files_combined "${file_switches}" ; then
            echo "ERROR: $file"
            has_error=1
        fi
    fi

    return ${has_error}
}

if ! check_files "$@" ; then
    echo "To ignore, use --no-verify"
fi

exit $has_error
